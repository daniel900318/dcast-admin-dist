<!doctype html>
<html>

<head>
  <base href="/">
  <meta charset="utf-8">
  <title>Video Streaming</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="icon" type="image/png" href="./favicon.png" />
  <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" href="https://code.getmdl.io/1.2.1/material.light_blue-amber.min.css" /> -->
</head>

<body style="overflow: hidden;">
  <dc-app>Loading...</dc-app>
  <dialog-outlet></dialog-outlet>
<script type="text/javascript" src="vendor-dc8441f3dd1b9e419e93.js"></script><script type="text/javascript" src="main-dc8441f3dd1b9e419e93.js"></script></body>

</html>
